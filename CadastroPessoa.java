import java.io.*;

public class CadastroPessoa{

  public static void main(String[] args) throws IOException{
    //burocracia para leitura de teclado
    InputStream entradaSistema = System.in;
    InputStreamReader leitor = new InputStreamReader(entradaSistema);
    BufferedReader leitorEntrada = new BufferedReader(leitor);
    String entradaTeclado;

    //instanciando objetos do sistema
    ControlePessoa umControle = new ControlePessoa();
    Pessoa umaPessoa = new Pessoa();
	double opcao = 10.0;
	 
	do{
		System.out.println("\nBem vindo ao Cadastro de Pessoas! \n Selecione uma opcao: \n1)Cadastrar uma pessoa \n2)Pesquisar uma pessoa \n3)Remover uma pessoa \n  ");
		entradaTeclado = leitorEntrada.readLine();
		String numeroOpcao = entradaTeclado;
		opcao = Double.parseDouble(numeroOpcao);
	
		if(opcao == 1.0){
		System.out.println("Digite o nome da Pessoa: \n");
		entradaTeclado = leitorEntrada.readLine();
		String umNome = entradaTeclado;
		umaPessoa.setNome(umNome);
		
		System.out.println("Digite a Idade da Pessoa: \n");
		entradaTeclado = leitorEntrada.readLine();
		String umaIdade = entradaTeclado;
		umaPessoa.setIdade(umaIdade);
		
		System.out.println("Digite o endereco da Pessoa: \n");
		entradaTeclado = leitorEntrada.readLine();
		String umEndereco = entradaTeclado;
		umaPessoa.setEndereco(umEndereco);
		
		System.out.println("Digite o email da Pessoa: \n");
		entradaTeclado = leitorEntrada.readLine();
		String umEmail = entradaTeclado;
		umaPessoa.setEmail(umEmail);
		
		System.out.println("Digite o telefone da Pessoa:\n");
		entradaTeclado = leitorEntrada.readLine();
		String umTelefone = entradaTeclado;
		umaPessoa.setTelefone(umTelefone);
			
		System.out.println("Digite RG da Pessoa:\n");
		entradaTeclado = leitorEntrada.readLine();
		String umRg = entradaTeclado;
		umaPessoa.setRg(umRg);
		
		System.out.println("Digite o CPF da Pessoa: \n");
		entradaTeclado = leitorEntrada.readLine();
		String umCpf = entradaTeclado;
		umaPessoa.setCpf(umCpf);
				
		System.out.println("Digite o sexo da Pessoa:\n");
		entradaTeclado = leitorEntrada.readLine();
		String umSexo = entradaTeclado;
		umaPessoa.setSexo(umSexo);
		
		String mensagem = umControle.adicionar(umaPessoa);
		System.out.println("=================================");
		System.out.println(mensagem);
		
		} else if(opcao ==2.0){
			System.out.println("Digite o nome da pessoa a pesquisar:\n");
			entradaTeclado = leitorEntrada.readLine();
			String nome = entradaTeclado;
			Pessoa retorno = umControle.pesquisar(nome);
			System.out.println(retorno);
			} else if(opcao == 3.0){
				System.out.println("Digite o nome da pessoa a remover:\n");
				entradaTeclado = leitorEntrada.readLine();
				String removePessoa = entradaTeclado;
				String alerta = umControle.remover(umaPessoa);
				System.out.println(alerta);
				}
			
			
			
		System.out.println("\nDeseja executar novamente? \n1)Sim \n2)Nao");
		entradaTeclado = leitorEntrada.readLine();
		numeroOpcao = entradaTeclado;
		opcao = Double.parseDouble(numeroOpcao);
		
		if(opcao==1.0){
			}else if(opcao==2.0){
				opcao = 0.0;
				}
				
		
	} while(opcao !=0);
    
    System.out.println("=)");	
    	 

  }

}
